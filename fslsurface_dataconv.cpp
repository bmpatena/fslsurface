//
//  fslsurface_dataconv.cpp
//  fslsurface
//
//  Created by Brian Patenaude on 5/30/11.
//  Copyright 2011 NRI. All rights reserved.
//

#include "fslsurface_dataconv.h"

//3rd party includes
#include "newmatap.h"


//fslsurface include
#include "fslsurface.h"
#include "fslsurfacefns.h"

#include "fslsurface_structs.h"

//STL includes
#include <vector>

using namespace std;
using namespace NEWMAT;
using namespace mesh;
//MAY WANT TO REMOVE DEPENDENCY ON THIS LIBRARY IN THE FUTURE, we'll see

//STL
using namespace std;


namespace fslsurface_name {
    
    template<class T, class T2>
    ReturnMatrix getScalars( fslSurface<T,T2> & surf, unsigned int index )
    {
        Matrix Scalars(surf->getNumberOfVertices(),1); 
        unsigned int count = 0;
        for (typename vector<T>::iterator i= surf->const_scbegin(index); i!= surf->const_scend();++i,++count)
        {
            Scalars.element(count, 0) = *i;
        }

        Scalars.Release();
        return Scalars;
    }
    
    template<class T,class T2>
    void convertTo_Mesh( Mesh * mesh, fslSurface<T,T2>* surf )
    {
        //create mesh if it doesn't exists
        if (mesh == NULL)
        {

            mesh = new Mesh();
        }

        //clear anything that might be there
        mesh->_points.clear();
        mesh->_triangles.clear();
        unsigned int count=0;
        for ( typename std::vector< fslsurface_name::vertex<T> >::const_iterator  i= surf->const_vbegin(); i!= surf->const_vend();++i,++count)
        {
            mesh->_points.push_back( new Mpoint(i->x, i->y, i->z, count));
        }
        
        for ( typename vector<T2>::const_iterator i = surf->const_facebegin(); i != surf->const_faceend(); i+=3)
        {
            Triangle * t = new Triangle(mesh->get_point(*i), mesh->get_point(*(i+1)), mesh->get_point(*(i+2)));
            mesh->_triangles.push_back(t);
            
            //            mesh->_triangles.push_back(new Triangle());
        }
        
    }
    template void convertTo_Mesh<float,unsigned int>( Mesh * mesh, fslSurface<float,unsigned int>* surf );
    
    template<class T,class T2>
    void convertTo_fslSurface( fslSurface<T,T2>& surf, const Mesh & mesh )
    {
        
        //create mesh if it doesn't exists
      //  if (surf == NULL)
       //     surf = new fslSurface<T,T2>();
        vector<T> vertices(mesh.nvertices()*3);
        vector<T2> triangles(mesh._triangles.size()*3);
        
        typename vector<T>::iterator i_v = vertices.begin();
        for (vector<Mpoint *>::const_iterator i = mesh._points.begin(); i != mesh._points.end();++i,++i_v)
        {
            *i_v = static_cast<T>((*i)->get_coord().X);
            ++i_v;
            *i_v = static_cast<T>((*i)->get_coord().Y);
            ++i_v;
            *i_v = static_cast<T>((*i)->get_coord().Z);
            //cout<<"add vertex "<<
        }
        surf.setVertices(vertices);
        
        typename vector<T2>::iterator i_t = triangles.begin();
        for (list<Triangle* >::const_iterator i = mesh._triangles.begin(); i != mesh._triangles.end();++i,++i_t)
        {
            *i_t = static_cast<T2>(  (*i)->get_vertice(0)->get_no() );
            ++i_t;
            *i_t = static_cast<T2>(  (*i)->get_vertice(1)->get_no() );
            ++i_t;
            *i_t = static_cast<T2>(  (*i)->get_vertice(2)->get_no() );
        }
        surf.setFaces(triangles);
            
        //addin coordinate system
        vector<double> xfm(16,0);
        xfm[0]=xfm[5]=xfm[10]=xfm[15]=1.0;
        
        surf.addCoordSystem(xfm,"UNKNOWN" );

        
    }
    template void convertTo_fslSurface<float,unsigned int>( fslSurface<float,unsigned int>& surf, const Mesh & mesh );

    
    template<class T, class T2,class T3>
    void convertTo_fslSurface( fslsurface_name::fslSurface<T,T2>& dest_surf, const mesh::Mesh & mesh , const NEWIMAGE::volume<T3> & image_ref)
    {
        convertTo_fslSurface( dest_surf, mesh );
        dest_surf.clearCoordSystems();
        //assign appropriate coordinate system
        //        read_volume_hdr_only(*im, *i_imnames );
        Matrix m_vox2mm = image_ref.newimagevox2mm_mat();
        
        Matrix m_mm2vox(4,4);
        m_mm2vox=0;
        m_mm2vox.element(0, 0) = 1.0/image_ref.xdim();
        m_mm2vox.element(1, 1) = 1.0/image_ref.ydim();
        m_mm2vox.element(2, 2) = 1.0/image_ref.zdim();
        m_mm2vox.element(3, 3) = 1.0;
        
        
        Matrix xfm_nii = m_vox2mm * m_mm2vox;
        vector<float> fmat;
        for (int i = 0 ; i < 4; ++i)
            for (int j = 0 ; j < 4; ++j)
                fmat.push_back(xfm_nii.element(i, j));
        apply_xfm(dest_surf, fmat);

        vector< float > csys_eye(16,0);
        csys_eye[0] = csys_eye[5]= csys_eye[10]= csys_eye[15]=1;

        //	//cout<<"add coord system "<<i_xfms->at(0)<<endl;
        dest_surf.addCoordSystem( csys_eye, "NIFTI_XFORM_SCANNER_ANAT");
        //--------------------------back to mni------------------------------
        //cout<<"setmni "<<endl;
        xfm_nii = xfm_nii.i();
        fmat.clear();
        for (int i = 0 ; i < 4; ++i)
            for (int j = 0 ; j < 4; ++j)
                fmat.push_back(xfm_nii.element(i, j));
        
        dest_surf.addCoordSystem( fmat, "FSL_XFORM_SCALEDMM");
        
        
    }

    template void convertTo_fslSurface<float,unsigned int,float>( fslsurface_name::fslSurface<float,unsigned int>& dest_surf, const mesh::Mesh & mesh , const NEWIMAGE::volume<float> & image_ref);


}