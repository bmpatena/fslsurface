#!/bin/sh
Usage(){
    echo "vertexAnalysis2.0 ref_surface degree_of_freedom design.mat design.con output_directory"
    exit 1
}

#input all surfaces
#input reference surfaces, can replace this by custom model, but good for first comparison
REF=$1
shift 
DOF=$1
shift 1
DESIGN=$1
shift 1
CONTRAST=$1
shift 1
outdir=$1
shift 1

while [ -d $outdir ] ; do  
    outdir=+${outdir}
done

mkdir $outdir



for surf in $@ ;do 
    fsurf=`basename $surf .vtk`
    fsurf=`basename $fsurf .gii`
    echo $surf $fsurf 
    #Register images to template , running dof6 and dof7
    ${FSLDIR}/bin//fslsurfacemaths $surf -reg_lq $REF $DOF ${outdir}/${fsurf}_2_ref_dof${DOF}.mat ${outdir}/${fsurf}_2_ref_dof${DOF}.gii

    #calculate displacement vectors, project onto normal, calculate magnitude and store on image

    ${FSLDIR}/bin//fslsurfacemaths ${outdir}/${fsurf}_2_ref_dof${DOF}.gii -sub $REF -copy_verts2vec -copy_verts $REF -projVecOntoNormals 0 -copy_scalar2image3D 0 ${outdir}/${fsurf}_2_ref_dof${DOF}_thick ${outdir}/${fsurf}_2_ref_dof${DOF}_thick.gii

    echo ${outdir}/${fsurf}_2_ref_dof${DOF}_thick >>${outdir}/im_surf_list.txt 

done


fslmerge -t ${outdir}/all_dof${DOF}_thick `cat ${outdir}/im_surf_list.txt`
fsl_glm -i ${outdir}/all_dof${DOF}_thick -d $DESIGN -c $CONTRAST -o ${outdir}/all_dof${DOF}_thick_betas --out_p=${outdir}/all_dof${DOF}_thick_p --demean

ncons=`${FSLDIR}/bin/fslnvols  ${outdir}/all_dof${DOF}_thick_p`
#if [ $ncons = 1 ] ; then

#else 
    mkdir ${outdir}/split
    fslsplit ${outdir}/all_dof${DOF}_thick_p ${outdir}/split/all_thick_cons_p -t
#fi

#do FDR thresholding

con=0
while [ $con -lt $ncons ] ; do 
    echo "PROCESS CON $con $ncons"
    thr=`fdr -i ${outdir}/split/all_thick_cons_p000${con} -q 0.05 | sed -n '2p'`
    echo "thr $thr"

    
#thr=`fdr -i all_thick_${si}_${st}_dof${dof}_p_0000 -q 0.05 | sed -n '2p'`
#echo "thr $thr"
if [ $con = 0 ] ; then 
    ${FSLDIR}/bin//fslsurfacemaths ${REF} -normals -copy_image3D2scalar ${outdir}/split/all_thick_cons_p000${con} thickness_p_${con} -sc_uthr $thr ${outdir}/all_dof${DOF}_thick_p_fdr_thr.gii
else
echo "add con"
    ${FSLDIR}/bin//fslsurfacemaths ${outdir}/all_dof${DOF}_thick_p_fdr_thr.gii -copy_image3D2scalar ${outdir}/split/all_thick_cons_p000${con} thickness_p_${con} -sc_uthr $thr ${outdir}/all_dof${DOF}_thick_p_fdr_thr.gii
fi
    echo "thrvol" `fslstats ${outdir}/split/all_thick_cons_p000${con} -u $thr -V`
    let con+=1
done

#echo "thr $thr"
#${FSLDIR}/bin//fslsurfacemaths ${REF} -normals -copy_image3D2scalar all_thick_${si}_${st}_dof${dof}_p_0000 fdr_thresh_p -sc_uthr $thr all_thick_${si}_${st}_dof${dof}_p_0000_thr.gii

#thr=`fdr -i all_thick_${si}_${st}_dof${dof}_p_0001 -q 0.05 | sed -n '2p'`
#echo "thr $thr"

#${FSLDIR}/bin//fslsurfacemaths ../../models/${si}_${st}_mean.gii -normals -copy_image3D2scalar all_thick_${si}_${st}_dof${dof}_p_0001 fdr_thresh_p all_thick_${si}_${st}_dof${dof}_p_0001.gii
#${FSLDIR}/bin//fslsurfacemaths ../../models/${si}_${st}_mean.gii -normals -copy_image3D2scalar all_thick_${si}_${st}_dof${dof}_p_0001 fdr_thresh_p -sc_uthr $thr all_thick_${si}_${st}_dof${dof}_p_0001_thr.gii

#${FSLDIR}/bin//fslsurfacemaths all_thick_${si}_${st}_dof${dof}_p_0000_thr.gii -sc_sub_sc 0 all_thick_${si}_${st}_dof${dof}_p_0001_thr.gii 0 con_0_sub_1_thr all_thick_${si}_${st}_dof${dof}_p_con_0_sub_1_thr.gii

