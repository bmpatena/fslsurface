#ifndef _fslSurface
#define _fslSurface

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <stdio.h>
#include <algorithm>
#include <map>
#include <list>
//#include "fslsurfacefns.h"
#include "fslsurface_structs.h"
//extern "C" {	
//#include <gifti/gifti_io.h>
//}

namespace fslsurface_name{
	//using namespace mesh;
	
	class fslSurfaceException : public std::exception{
		
	public:
		const char* errmesg;
		fslSurfaceException(const char* msg)
		{
			errmesg=msg;
		}
		virtual const char* what() const throw()
		{
			std::cout<<errmesg<<std::endl;
			return errmesg;
		}
//	private:
		
	};
	
//---------------------------float3-----------------------------//
	template<class T>
	inline vec3<T> subtract( const vec3<T> & a, const vec3<T> & b );
	template<class T>
	inline vec3<T> normal( const vec3<T> & a, const vec3<T> & b );
	//---------------------------end float3-----------------------------//
	
	
//	extern "C" {
//
//		void giftWrapper_read_Image(const std::string & filename, gifti_image* gii_surf);
//	
//	}
//	class fslSurfaceIO;
//	class fslSurfaceFNS;
//	enum MarchingCubesMode {EQUAL_TO, GREATER_THAN, GREATER_THAN_EQUAL_TO};

	
	template<class T,class T2>
	class fslSurface {
		
		
	public:
	
		
		enum DataType { ascii, littleEndian, bigEndian };
		enum NumFormat { float32, float64 };
	
        //-------operators------//
        void operator+=(const fslSurface<T,T2> &surf);
        void operator+=(const vec3<T> &cog);

        void operator-=(const fslSurface &surf);
        void operator*=(const T &mul);
        void operator/=(const T &div);
        
        fslSurface<T,T2> operator +(const  fslSurface<T,T2> & surf );
        fslSurface<T,T2> operator -(const  fslSurface<T,T2> & surf );
        fslSurface<T,T2> operator *(const  T & surf );
        fslSurface<T,T2> operator /(const  T & surf );

        void append (const  fslSurface<T,T2> & surf );

        
      //  void operator+=(const fslSurface &surf);
      //  void operator+=(const fslSurface &surf);

        //-----------------------//
        
        
		fslSurface();
        fslSurface( const fslSurface<T,T2> & surf );

		//fslSurface(const std::string & filename);
		virtual ~fslSurface();
		//vertex<T> vbegin3();
        //vertex access
        std::vector<T> getBounds() const;
        //-----------FUNCTIONS TO ACCESS USING ITERATORS, will be faster for multiple access----------------//

        typename std::vector< fslsurface_name::vertex<T> >::iterator vbegin();
        typename std::vector< fslsurface_name::vertex<T> >::const_iterator const_vbegin() const;
		typename std::vector< fslsurface_name::vertex<T> >::iterator vend() ;
        typename std::vector< fslsurface_name::vertex<T> >::const_iterator const_vend() const;
        
        
        //face access 
        typename std::vector< T2 >::const_iterator const_facebegin() const;
        typename std::vector< T2 >::const_iterator const_faceend() const;
        
        std::vector< std::vector<T2> > getFacesAsVectorOfVectors( ) const;

        
        //assumes triangles 
        //get specific face as a vec3 (x,y,z are vertex indices)
        void eraseTriangle( const T2 & index );
        typename fslsurface_name::vec3<T2> getFace( const unsigned int & index, int face_size = 3 ) const;
        typename std::vector<T2> getFaces() const;
        std::vector< fslsurface_name::vec3<T> > getFaceVertices( const unsigned int & index, int face_size =3 ) const;

        std::vector< T > getFaceVerticesUnwrapped( const unsigned int & index, int face_size =3 ) const;
        std::vector< std::list<T2> >getVertexAdjacencies() const ;
        
        
        typename std::vector< T >::const_iterator const_scbegin(const unsigned int & index ) const;
        typename std::vector< T >::const_iterator const_scend(const unsigned int & index) const;
        
        typename std::vector< std::vector< T > >::const_iterator const_scbegin( ) const;
        typename std::vector< std::vector< T > >::const_iterator const_scend() const;
        
        
        typename std::vector< std::string >::const_iterator const_scnamesbegin( ) const;
        typename std::vector< std::string >::const_iterator const_scnamesend() const;
        

        

        typename std::vector< T >::const_iterator const_vecbegin(const unsigned int & index ) const;
        typename std::vector< T >::const_iterator const_vecend(const unsigned int & index) const;


		typename std::vector< std::list<T2> >::const_iterator const_adjvertsbegin() const ;

        //-----------------------------------------------------------------------------------------------//
        //-----------FUNCTIONS FOR Random access to data ACCESS, will be faster for multiple access----------------//

        //various attributes
		vec3<T> getCOG();
		unsigned int getNumberOfFaces() const  { return faces_.size()/3; }
		unsigned int getNumberOfVertices() const { return vertices_.size(); }
		unsigned int getNumberOfScalarData() const { return scalar_data.size(); }
        unsigned int getNumberOfVectorData() const { return vector_data.size(); }
        
        //access to data (e.g. vertex, scalars, triangles, etc..)
        fslsurface_name::vec3<T> getVertexCoord(const unsigned int & index);
        vertex<T> getVertex( const unsigned int & vert_ind ) { return vertices_[ vert_ind ]; }
        std::vector<T> getVerticesAsVector() const;

        
        
        
        T getScalar( const unsigned int & sc_ind, const unsigned int & vert_ind ) const;
        std::string getScalarName( const unsigned int & sc_ind );

        void getScalarsAsTimeSeries( std::vector< std::vector<T> > & sc_ts  ) const;

        std::vector<T> getScalars(const unsigned int & index ) const;
        std::vector<T> getScalars(const std::string & name ) const;
        std::vector<int> getNonVertIntScalars(const unsigned int & index ) const;
        std::vector<int> getNonVertIntScalars(const std::string & name ) const;
        std::vector<float> getNonVertFloatScalars(const unsigned int & index ) const;
        std::vector<float> getNonVertFloatScalars(const std::string & name ) const;

        
		std::vector<int> getScalarIndices( const int & index );

        
        //-----------------------------------------------------------------------------------------------//
        //---------------------------------FUNCTIONS TO SET, ADD OR INSERT DATA-------------------------------------------------------------//
        void setVertices( const std::vector<T> &  verts );
        void setVertices( const std::vector< fslsurface_name::vec3<T> > &  verts );

        void setVert(const unsigned int & index, const fslsurface_name::vec3<float> & vert_coord);

		void setFaces( const std::vector<T2> &  faces_in );
		void setFaces( const std::vector< std::vector<T2> > &  faces_in );
        std::vector< std::vector<T2> > getFacesAsVectorOfVectors( );

        //set the active scalars by copying the scalar value into the vertex scalar field
		void setScalars(const int & index );
        void copyScalars( const fslSurface<T,T2> & surf );
        void replaceScalars(const std::vector< std::vector<T> > & scalars , const std::vector< std::string > & sc_names);
        void clearScalars();
		void insertScalars(const std::vector<T> & scalars, const unsigned int & index, const std::string & sname);
		void addScalars(const std::vector<T> & scalars, const std::string & sname );
        void addScalars(const T & scalars, const std::string & sname );

        void insertNonVertScalars( const std::vector<int> & scalars, const unsigned int & index, const std::string & sname );
        void insertNonVertScalars( const std::vector<float> & scalars, const unsigned int & index, const std::string & sname );
        void printScalars(const unsigned int & index );
        
		void addVectors(const std::vector<T> & vec, const std::string & sname, const unsigned int & index );
        
        

        //-----------------------------------------------------------------------------------------------//

        //---------------------------------SCALAR MANIPULATOR FUNCTIONS--------------------------------------------------------------//
        
        void subtractScalars(const unsigned int & index, const T & value );
        void multiplyScalars(const unsigned int & index, const T & value );
        void Tsum( const std::string & sc_name);
    	void scaleScalarsByIndices();

        void binariseScalars(const unsigned int & index, const T & threshold );
        void thresholdScalars(const unsigned int & index, const T & threshold );
        void upperThresholdScalars(const unsigned int & index, const T & threshold );
        
        //-----------------------------------------------------------------------------------------------//


        void copyNormalsToVectors(const unsigned int & index);
        void copyVerticesToVectors();
        void copyVerticesToVectors( const std::string & name );
        void copyVertices( const fslSurface<T,T2> & surf );

        
        //-------------------vertex operators---------------------------------------//
        float L2norm( const unsigned int & index0, const unsigned int & index1);
        fslsurface_name::vec3<float> subtractVerts(const unsigned int & index0, const unsigned int & index1);

        
        //------------------------------------------coordinate system functions ------------------------------------------//
        unsigned int getNumberOfCoordinateSystems();
		void addCoordSystem(const std::vector<double> & xfm, const std::string & xfm_space );
		void addCoordSystem(const std::vector<float> & xfm, const std::string & xfm_space );
        void copyCoordSystems(const fslSurface<T,T2> & surf);
        void clearCoordSystems();
        std::vector<double> getCoordinateSystem( const std::string & name);

		
        
        
		unsigned int getNumberOfDataTableEntries();
		std::map<int,float4> getDataTable();
		
		void computeAdjacentTriangles();
		void computeAdjacentVertices(const bool & bidirectional);



		void calculateNormals(bool normalize = true, bool reverse_winding=false);

   
		//------------------------------------------------------------//
		static void copy_coordsystem(fslSurface & surf_dest, const fslSurface & surf_src);
		
		
		////////-------------FRIEND FUNCTIONS FSLSURFACE_IO-------------/////////
		template<class U,class U2>
		friend int read_surface( fslSurface<U,U2>& surf, const std::string & filename);
		template<class U,class U2>
		friend int readGIFTI( fslSurface<U,U2>& surf, const std::string & filename );
		template<class U,class U2>
		friend int readVTK( fslSurface<U,U2>& surf, const std::string & filename );
		template<class U,class U2>
		friend int readPLY( fslSurface<U,U2>& surf, const std::string & filename );
		template<class U,class U2>
        friend int writeGIFTI(  const fslSurface<U,U2>& surf, const std::string & filename);
//		  friend int writeGIFTI(  const fslSurface<U,U2>& surf, const std::string & filename,int enc);
        template<class U,class U2>
        friend int writeVTK(  const fslSurface<U,U2>& surf, const std::string & filename);
		////////-------------FRIEND FUNCTIONS FSLSURFACE_FNS-------------/////////
		template<class U,class U2, class U3>
		 friend void runMarchingCubesOnAllLabels( fslSurface<U,U2>& surf , const U3* image,  const fslsurface_name::image_dims & dims, const U3 & thresh_in);
		template<class U,class U2, class U3>
		 friend void marchingCubes( fslSurface<U,U2>& surf , const U3* images,  const fslsurface_name::image_dims & dims, const U3 & thresh, const U & label, const MarchingCubesMode & mode);
		template<class U,class U2>
		friend void maskScalars( fslSurface<U,U2>& surf , const unsigned int & sc_index, const short* mask,fslsurface_name::image_dims dims, bool  setToCurrentSc );
		template<class U, class U2>
		friend void maskSurface( fslSurface<U,U2>& surf , const short* mask, const fslsurface_name::image_dims  dims, bool  setToCurrentSc );
		template<class U,class U2, class U3>
		friend void mapScalars( fslSurface<U,U2>& surf , const unsigned int & sc_index, const std::map<U3,U3> & scMap, bool setToCurrentSc  ); 		
		template<class U,class U2,class U3>
		friend void apply_xfm( fslSurface<U,U2>& surf, const std::vector<U3> & xfm );
    
        template <class U,class U2>
        friend void cluster( fslSurface<U,U2>& surf , const unsigned int & index, const U & threshold );
        
        template <class U,class U2>
        friend void sc_smooth_mean_neighbour( fslSurface<U,U2>& surf , const unsigned int & index );

        template <class U,class U2>
        friend void sc_smooth_gaussian_geodesic( fslSurface<U,U2>& surf ,const unsigned int & index, const U & st_dev , const U & extent , bool run4D);
        
        template <class U,class U2>
        friend void sc_smooth_gaussian_geodesic_fast( fslSurface<U,U2>& surf ,const unsigned int & index, const U & st_dev , const U & extent , bool run4D);
        
        
        template<class U, class U2>
        friend void multiplyScalarsByScalars(fslSurface<U,U2>& surf , const unsigned int & index, const fslSurface<U,U2>& surf2 , const unsigned int & index1);
        
        template<class U, class U2>
        friend void subtractScalarsFromScalars(fslSurface<U,U2>& surf , const unsigned int & index, const fslSurface<U,U2>& surf2 , const unsigned int & index1, std::string name );

        
        template<class U,class U2>
        friend void projectVectorsOntoNormals(fslSurface<U,U2>& surf ,const unsigned int & index);
        
		////////-------------FRIEND FUNCTIONS FSLSURFACE_GL-------------/////////
		
		template<class U,class U2>
		friend void glBufferData_Vertices( const fslSurface<U, U2>& surf, const GLuint & vbo );  
		template<class U,class U2>
		friend void glBufferSubData_Vertices( const fslSurface<U, U2>& surf, const GLuint & vbo )  ;
		template<class U,class U2>
		friend void glBufferData_Faces( const fslSurface<U, U2>& surf, const GLuint & vbo )  ;
		template<class U,class U2>
		friend void glBufferSubData_Faces( const fslSurface<U, U2>& surf, const GLuint & vbo )  ;
		template<class U,class U2>
		friend void render( const fslSurface<U, U2>& surf, const GLint & vertexLoc , const GLint &  normalLoc, const GLint & scalarLoc, const GLuint & vbo_verts, const GLuint & vbo_ele_array ) ;
		
        template<class U,class U2>
        friend void depthSortTriangles( const fslSurface<U,U2>& surf, const GLuint & vbo_verts, const GLuint & vbo_tris);

		//friend class fslSurfaceIO;
		//friend class fslSurfaceFNS;x

	protected:
		
		
	private:


		//DataType machineEndianness();
		
		std::vector< vertex<T> > vertices_;
		std::vector<T2> faces_;//assumes triangles
		std::vector< vec3<T> > tangents;

		std::vector< std::list<T2> > adj_tris_;
		std::vector< std::list<T2> > adj_verts_;
        bool adj_verts_bi;
		std::vector< std::vector<double> > v_coord_sys; 
		std::string csys_dspace;
		std::vector< std::string > v_csys_xfmspace; 

		//if intending to use the openGL use unsigned int (suggested anyways)
		std::vector< std::vector<T> > vector_data;
        std::vector< std::string > vector_names;

		std::vector< std::vector<T> > scalar_data;
		std::vector< std::string > scalar_names;
		std::vector< std::vector<int> > scalar_indices;

		std::map<int,float4> dataTable;
		
        std::vector< std::vector<float> > nonvert_float_sc_data;
        std::vector< std::string > nonvert_float_sc_data_names;
        
        std::vector< std::vector<int> > nonvert_int_sc_data;
        std::vector< std::string > nonvert_int_sc_data_names;

//		std::vector<std::string> element_names;	
//		std::vector< std::vector<std::string> > element_types;//assuming same type
//		std::vector< std::vector<std::string> > element_prop_names;	
//		std::vector< int > element_num_props;	
//		std::vector< std::pair<std::string,std::string> > list_types;


		std::vector<unsigned int> element_sizes;
		
		NumFormat num_format;
		DataType format;
		//FileType file_type;
		std::string ply_version;
		
		unsigned int N_vertices,N_triangles;
		vec3<T> cog;
		std::map<int,std::string> datatype_str;

		
		//		vector<string> celement_names;
		//	DataType format;
//		int format;
		//EndianType endian_format;

	};
}
#endif
