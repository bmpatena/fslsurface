//
//  main.cpp
//  fslsurfacemaths
//
//  Created by Brian Patenaude on 3/27/11.
//  Copyright 2011 NRI. All rights reserved.
//

//FSL includes
#include <newimage/newimageall.h>
#include <meshclass/mesh.h>
//#include <fslsurface/fslsurface.h>
//#include <fslsurface/fslsurfacefns.h>
//#include <fslsurface/fslsurfaceio.h>
//#include <fslsurface/fslsurface_first.h>
#include <fslsurface_dataconv.h>
#include <fslsurface.h>
#include <fslsurfacefns.h>
#include <fslsurfaceio.h>
#include <fslsurface_first.h>
#include <fslsurfacegl.h>
//#include <AGL/agl.h>
#include <iostream>
#include <sstream>



using namespace fslsurface_name;
using namespace NEWIMAGE;
using namespace mesh;
using namespace std;

float string2float(const string & snum ){
    stringstream ss;
    ss<<snum;
    float num;
    ss>>num;
    return num;
}

void Usage(){
    cout<<endl<<endl;
    cout<<"--------------------------- Usage ----------------------------------------------"<<endl;
    cout<<endl;
    cout<<" fslsurfacemaths <first_input_surface> [operations and inputs] <output> \n or \n fslsurfacemaths -reconFromBvars fike.bvars [operations and inputs] <output> \n\n"<<endl;
    cout<<"Operations: (some inputs can be either surfaces or numbers) \n\n "<<endl;
    cout<<"    -readGIFTI <surface.gii> read GIFTI files, will obey GIFTI IO rules for append and overwriting."<<endl;
    cout<<"    -appendLabels <freesurfer_label_file> : appends a scalar entry to gifti file."<<endl;
    cout<<"    -appendLabelsList <list_of_freesurfer_label_file> : Combines all scalars into a single atlas, if overlap, later labels take precedence."<<endl;
    cout<<"    -appendLabelsListToAtlas <fs_color_table> <list_of_freesurfer_label_file> : Combines all scalars into a single atlas, if overlap, later labels take precedence."<<endl;

    cout<<"    -add <surface> : Add surface vertices to existing vertices."<<endl;
    cout<<"    -sub <surface> : Subtract surface vertices to existing vertices."<<endl;
    cout<<"    -mul <scalar>  : Multiply surface vertices by a scalar."<<endl;
    cout<<"    -div <surface> : Add surface vertices to existing vertices."<<endl;
    cout<<"    -bin <scalars index>  : use (scalars>0) to binarise"<<endl;
    cout<<"    -sc_sub <index> <value>: subtract value from scalars."<<endl;
    cout<<"    -sc_mul <index> <value>: multiply scalar by value."<<endl;
    cout<<"    -sc_mul_sc <index> <surf2> <index2>: multiple scalars of 2 surfaces."<<endl;
    cout<<"    -sc_sub_sc <index> <surf2> <index2> <name_of_new>: subtract scalars of 2 surfaces."<<endl;
    cout<<"    -sc_append <surf2> : append scalars from surface."<<endl;
    cout<<"		-sc_Tsum <scalar name>: sum all scalars "<<endl;
    cout<<"		-sc_scaleByIndex : scale scalars by their index+1 (assuming start index is 0). Useful for creating atlases "<<endl;
    cout<<"		-sc_2_nifti2D : converts the scalars to a NIFTI files, vertex index as x and y as scalar index "<<endl;
    cout<<"    -sc_bin <index> : Add surface vertices to existing vertices."<<endl;
    
    cout<<"    -sc_thr <thresh> : Thresh scalars at index zero."<<endl;
    cout<<"    -sc_uthr <thresh> : Add surface vertices to existing vertices."<<endl;
    
    cout<<"		-addVertexIndexAsScalars: Append the vertex indices as the scalar values"<<endl;

    cout<<"    -cluster <index> <threshold> : Cluster scalars at index with threshold."<<endl;
    cout<<"     -sc_smooth <index> : Smooth scalars at index."<<endl;
    cout<<"     -sc_smooth_gauss_geodesic <index> <variance(mm)> : Smoot scalars at index."<<endl;
    cout<<"     -sc_smooth_gauss_geodesic_4D <variance(mm)> : Smoot scalars at index."<<endl;
    
    cout<<"     --projVecOntoNormals <index> : project vector onto normals"<<endl;
    cout<<"    -applyxfm <xfm.mat> : Apply a linear XFM matrix to the vertices and replace existing vertices (same format as FLIRT)."<<endl;
    cout<<"    -reg_lq <surface> <dof> <output_name_xfm.mat> : Calculate a linear XFM matrix using Least-Squares (vertex differences). XFM'd vertices replace original. Output matrix stored in 'output_name_xfm' (same format as FLIRT)."<<endl;
    cout<<"     -copy_CoordSystems <surf> : Copy coordinate systems for a reference surface. Typically used along side -applyxfm"<<endl;
    cout<<"     -normals : Calculate normals and store in vector field."<<endl;
    cout<<"     -vmag <vector_index> <scalar_name>: Compute vector magnitude and store in scalars"<<endl;
    cout<<"    -copy_verts <surface.gii> : Copy the vertices from the following surface."<<endl;
	cout<<"    -copy_verts_from_text : Copy the vertices from a text file. 1 vertex per line"<<endl;
    cout<<"    -copy_verts2vec : inserts the vertices as vector into the vector field (no input)."<<endl;
    cout<<"    -copy_scalar2image3D <scalar_index> <output_image> : copies vector into a Nx1x1 NIFTI image."<<endl;
    cout<<"    -copy_scalars2image4D <output_image> : copies vector into a Nx1x1xNt NIFTI image."<<endl;
    
    cout<<"    -copy_image3D2scalar <image_name> <scalar_name> : insert a Nx1x1 NIFTI into the vector field. Also specify the names of the scalars"<<endl;
    cout<<"    -copy_image4D2scalar <image_name> <scalar_name> : insert a Nx1xM NIFTI into the vector field. Also specify the names of the scalars"<<endl;
    
    cout<<"    -copy_vec2image3D <vec_index> <output_image> : copies vector into a Nx3x1 NIFTI image."<<endl;
    cout<<"    -copy_image3D2vec <image_name> <vector_name> <vec_index> <xyz>: insert a Nx3x1 NIFTI into the vector field. The last arguments specifies which coomponents of the vector to copy over"<<endl;
    cout<<"     -sampleTimeSeries <4D image> <scalar_name>"<<endl;
    cout<<"     -sampleImage <3D image> <inter_type> <scalar_name> : interp_type can be trilinear or nn "<<endl;

    cout<<"     -vertexMVGLM : <list_of_surfaces> <design_matrix>"<<endl;
    cout<<"     -FtoP : convert F values to scalar values"<<endl;
    cout<<"     -fillMesh <ref_im> <label> <save_name> : Draw anf Fill in Surface."<<endl;
    cout<<"     -drawMesh <ref_im> <label> <save_name> : Draw surface."<<endl;

    cout<<"    -reconFromBvars <file.bvars> : Reconstructs surfaces from .bvars files output (only first surface)."<<endl;
    cout<<"    -reconAllFromBvarsAndSave <file.bvars> <output_basename> : Reconstructs all surfaces from .bvars and save"<<endl;
    cout<<"    -marchingCubes <image> <threshold> <label> <mode> : run marching cubes."<<endl;
    
    cout<<"     -xfmToFSLScaledMM : Transform to scaled mm coordinates."<<endl;

    cout<<"     -appendFreesurferLabelFile <list of ascii labels>"<<endl;

    cout<<"     -splitFreesurferLabelFiles <list of ascii labels> <output_dir>  : split the freesurfer ROIs into separate connected regions"<<endl;

    cout<<endl;
    cout<<"------------------------------------------------------------------------------"<<endl;
    
}

string itos(const int & val )
{
	stringstream ss;
	ss<<val;
	string sval;
	ss>>sval;
	return sval;

}
bool search_and_remove_from_ordered_list( list<unsigned int> & lordered, const unsigned int & value )
{
	//list are also unique
	list<unsigned int>::const_iterator i_val = lordered.end();
	//check begining and end
	if ( (lordered.front() > value ) || (lordered.back() < value ) )
	{
		return false;
	}

	//check boundary conditions
	if (lordered.front() == value)
	{
		lordered.pop_front();
		return true;
	}

	if (lordered.back() == value)
	{
		lordered.pop_back();
		return true;
	}




	//lets find it
	auto i_search = lordered.begin();
	unsigned int left(0),right(lordered.size()-1);
	//cur index is 0->N-1
	unsigned int cur_index = (right - left)/2;
	advance(i_search,cur_index );

	//keep looping until or boundary indices are matches
	while ( (right-left) > 1 )
	{
		if (value == *i_search )
		{
			//found
			lordered.erase(i_search);
			return true;
		}else if (value > *i_search)
		{//move right
			left=cur_index;
			//right stays the same
			 int shift = (right-left)/2;
			cur_index+=shift;
			advance(i_search,shift);
		}else{//move left
			right=cur_index;
			int shift = (right-left)/2;
			cur_index-=shift;
			advance(i_search,-shift);

		}
	}


	//not found case
	return false;
}



int main (int argc, char * argv[])
{
    //Assume we're working with vertices will allow to change mode to scalars
    
    // insert code here...
    std::cout << "Welcome to FSL surface maths !\n";
    
    int i_arg=1;
    if (argc<2)
    {
        cout<<"Too few arguments."<<endl;
        Usage();
        return 1;
    }
    
    fslSurface<float,unsigned int> surf;// = new fslSurface<float,unsigned int>();
    if ( ( string(argv[i_arg]) != "-reconFromBvars") && (string(argv[i_arg]) != "-reconAllFromBvarsAndSave") && ( string(argv[i_arg]) != "-vertexMVGLM" )  && ( string(argv[i_arg]) != "-marchingCubes" ) )
    {
        read_surface(surf,argv[i_arg]);
        
        
        cout<<"done reading surface "<<endl;
        ++i_arg;
    }
    cout<<"iarg "<<i_arg<<" "<<argc<<endl;
    while (i_arg < (argc-1)) {
        cout<<"Enter Loop iarg "<<i_arg<<" "<<argc<<endl;
        cout<<"i_arg "<<i_arg<<" "<<argc<<" "<<argv[i_arg]<<endl;
        string command = string(argv[i_arg]);
        if (command == "-readGIFTI"){
            readGIFTI(surf, argv[i_arg+1]);
            i_arg += 2;
        }else if (command == "-appendLabelsList" )
        {
            string fnamelist=argv[i_arg+1];
            i_arg+=2;
            
            unsigned int n_labels=0;
            int label=1;
            vector<float> scalars_lb(surf.getNumberOfVertices(),0);
            ifstream flist(fnamelist.c_str());
            if (flist.is_open()){
                string fname;
                
                while (getline(flist,fname))
                {
                    bool found_n=false;

                    ifstream fin(fname.c_str());
                    cout<<"Adding label "<<fname<<" "<<label<<endl;
                    if (fin.is_open())
                    {
                        string line;
                        while (getline(fin,line))
                        {
                            if (line.substr(0,1)== "#")
                                continue;
                            else if (!found_n)
                            {
                                stringstream ss;
                                ss<<line;
                                ss>>n_labels;
                                found_n=true;
                                                                cout<<"there are "<<n_labels<<endl;
                            }else{
                                unsigned int index_lb;
                                stringstream ss;
                                ss<<line;
                                ss>>index_lb;
                                                                cout<<"Found index "<<index_lb<<endl;
                                scalars_lb[index_lb]=label;
                                
                            }
                            //                            cerr<<"line "<<line<<endl;
                        }
                        
                        fin.close();
                        ++label;
                    }else{
                        cerr<<"Could not open label file "<<endl;
                        exit (EXIT_FAILURE);
                    }
                }
                surf.addScalars(scalars_lb,fnamelist);
                
            }else
            {
                cerr<<"Could not open list of labels file "<<endl;
                exit (EXIT_FAILURE);

            }

        }else if (command == "-appendLabelsListToAtlas" )
        {
        	string ctable_name=argv[i_arg+1];
        	//read in colourtable and map RGB into sclaar value
        	map<string,unsigned long int> name2scalar;
        	{
        		ifstream fctab(ctable_name.c_str());
        		if (fctab.is_open())
        		{
        			string line;
        			while (getline(fctab,line))
        			{
        				stringstream ss(line);
        				if (line.substr(0,1) != "#")//igonre lines starting with #
        				{
        				string slabel;
        				int index;
        				float r,g,b;
        				ss>>index>>slabel>>r>>g>>b;
        				cout<<"reading ctab "<<slabel<<" "<<r<<" "<<g<<" "<<b<<" "<<(r + g*256 + b * 256 * 256)<<endl;
        				//using 2 decimal paces to represent rgb values
//                    			name2scalar.insert(pair<string,float>(slabel,roundf(r/255 * 100)/ 100 *1000 + roundf(g/255.0 *100)/100 * 1000000+ roundf(b/255.0 *100)/100 * 1000000000 ));
                    			name2scalar.insert(pair<string,float>(slabel,roundf(r + g*256 + b * 256 * 256 )));

        				}

        			}
        			fctab.close();
        		}
        	}


        	string fnamelist=argv[i_arg+2];
        	i_arg+=3;

        	unsigned int n_labels=0;
        	int label=1;
        	vector<float> scalars_lb(surf.getNumberOfVertices(),0);
        	ifstream flist(fnamelist.c_str());
        	if (flist.is_open()){

                string fname;
        		while (getline(flist,fname))
        		{
        			bool found_n=false;

        			//strip path and remove lh. and rh. (i.e. first 3 characters)
        			std::size_t found = fname.find_last_of("/\\");
        			string labelname;
        			            	//Need to handle the case where there is no path preceding the filenames
        			        		if (found == string::npos)
        			        		{
        			        			labelname= fname.substr(3);
        			        		}else{
        			        			labelname= fname.substr(found+4);
        			        		}

        			ifstream fin(fname.c_str());

        			label=name2scalar[labelname];
        			int r,g,b;
        			b=floor(label/256/256);
        			g=floor((label - b*256*256)/256);
        			r=label - b*256*256 -g * 256;
        			cout<<"Adding label "<<fname<<" "<<label<<" "<<labelname<<" "<<r<<" "<<g<<" "<<b<<endl;
        			if (fin.is_open())
        			{
        				string line;
        				while (getline(fin,line))
        				{
        					if (line.substr(0,1)== "#")
        						continue;
        					else if (!found_n)
        					{
        						stringstream ss;
        						ss<<line;
        						ss>>n_labels;
        						found_n=true;
        						cout<<"there are "<<n_labels<<endl;
        					}else{
        						unsigned int index_lb;
        						stringstream ss;
        						ss<<line;
        						ss>>index_lb;
//        						cout<<"Found index "<<index_lb<<endl;
        						scalars_lb[index_lb]=label;

        					}
        					//                            cerr<<"line "<<line<<endl;
        				}

        				fin.close();
        				++label;
        			}else{
        				cerr<<"Could not open label file "<<endl;
        				exit (EXIT_FAILURE);
        			}
        		}
        		cout<<"SCALARS "<<endl;
        	        		for (auto i = scalars_lb.begin(); i != scalars_lb.end(); ++i)
        	        		{
        	        			cout<<(*i)<<" ";
        	        		}
        	        		cout<<endl;
        		surf.addScalars(scalars_lb,fnamelist);

        	}else
        	{
        		cerr<<"Could not open list of labels file "<<endl;
        		exit (EXIT_FAILURE);

        	}

        }else if (command == "-appendLabels" ){
        	string fname=argv[i_arg+1];
        	unsigned int n_labels=0;
        	bool found_n=false;
        	ifstream fin(fname.c_str());
        	i_arg+=2;
        	if (fin.is_open())
        	{
        		vector<float> scalars_lb(surf.getNumberOfVertices(),0);
        		string line;
        		while (getline(fin,line))
        		{
        			if (line.substr(0,1)== "#")
        				continue;
                    else if (!found_n)
                    {
                        stringstream ss;
                        ss<<line;
                        ss>>n_labels;
                        found_n=true;
                        cout<<"there are "<<n_labels<<endl;
                    }else{
                        unsigned int index_lb;
                        stringstream ss;
                        ss<<line;
                        ss>>index_lb;
                        cout<<"Found index "<<endl;
                        scalars_lb[index_lb]=1;
                        
                    }
                    cerr<<"line "<<line<<endl;
                }
                
                fin.close();
                surf.addScalars(scalars_lb,fname);
            }else{
                cerr<<"Could not open label file "<<endl;
                exit (EXIT_FAILURE);
            }
            
        }else if (command == "-sub"){
            fslSurface<float,unsigned int> surf2;
            cout<<"read surface "<<endl;
            read_surface(surf2, argv[i_arg+1]);
            cout<<"read surface2 "<<endl;
            
            surf-=surf2;
            cout<<"done sub surface "<<endl;
            
            i_arg += 2;
        }else if (command == "-sc_sub"){
            
            surf.subtractScalars(atoi(argv[i_arg+1]),string2float(argv[i_arg+2]));
            cout<<"done sub surface "<<endl;
            
            i_arg += 3;
        }else if ( command == "-sc_Tsum" )
        {
        	surf.Tsum(argv[i_arg+1]);
        	i_arg+=2;
        }else if (command == "-sc_scaleByIndex" )
        {
        	surf.scaleScalarsByIndices();
        	++i_arg;
    }else if (command == "-sc_mul"){
            
            surf.multiplyScalars(atoi(argv[i_arg+1]),string2float(argv[i_arg+2]));
            cout<<"done mul surface "<<endl;
            
            i_arg += 3;
        }else if (command == "-sc_mul_sc"){
            fslSurface<float,unsigned int> surf2;
            cout<<"read surface "<<endl;
            read_surface(surf2, argv[i_arg+2]);
            cout<<"multiply sclars"<<endl;
            multiplyScalarsByScalars(surf,atoi(argv[i_arg+1]),surf2,atoi(argv[i_arg+3]));
            cout<<"done sc mul surface "<<argv[i_arg+3]<<endl;
            
            i_arg += 4;
            cout<<"done sc mul surface2 "<<argv[i_arg]<<endl;
            
        }else if (command == "-sc_sub_sc"){
            fslSurface<float,unsigned int> surf2;
            cout<<"read surface "<<endl;
            read_surface(surf2, argv[i_arg+2]);
            cout<<"subtract sclars"<<endl;
            string sc_name=argv[i_arg+4];
            
            subtractScalarsFromScalars(surf,atoi(argv[i_arg+1]),surf2,atoi(argv[i_arg+3]),sc_name);
            cout<<"done sc sub surface "<<argv[i_arg+3]<<endl;
            i_arg += 5;
            cout<<"done sc sub surface2 "<<argv[i_arg]<<endl;
            
        }else if ( command == "-sc_2_nifti2D" )
        {
        	if (surf.getNumberOfScalarData() == 0 )
        	{
        		cerr<<"ERROR : -sc_2_nifti2D : no scalars"<<endl;
        		return 1;
        	}
        	cout<<"nverts "<<surf.getNumberOfVertices()<<endl;
        	volume<float> imout(surf.getNumberOfVertices(), surf.getNumberOfScalarData(), 1);

        	vector<float> sc;
        	for (unsigned int i = 0 ; i < surf.getNumberOfScalarData(); ++i )
        	{
        		sc = surf.getScalars(i);
        		int x = 0;
        		for (auto i_sc = sc.begin(); i_sc != sc.end(); ++i_sc,++x )
        		{
        			imout.value(x,i,0) = *i_sc;
        		}
        	}
        	cout<<"save volume "<<endl;
        	cout<<argv[i_arg+1]<<endl;

        	save_volume(imout, argv[i_arg+1]);
        	i_arg += 2;

        }else if ( command == "-sc_append" ){

        	fslSurface<float,unsigned int> surf2;
        	cout<<"read surface "<<endl;
        	read_surface(surf2, argv[i_arg+1]);
        	for (unsigned int i = 0 ; i < surf2.getNumberOfScalarData(); ++i )
        	{
        		surf.insertScalars(surf2.getScalars(i), surf.getNumberOfScalarData(), surf2.getScalarName(i));
        	}

            i_arg += 2;

        }else if ( command == "-sc_resample" )
        	{
        	//input src sphere.reg
        	//input targ sphere.reg
        	//input targ base surface

        	 fslSurface<float,unsigned int> surfsrc;
        	 fslSurface<float,unsigned int> surftarg;

        	        	            cout<<"compare: read surface "<<endl;
        	        	 read_surface(surfsrc, argv[i_arg+1]);
        	        	 read_surface(surftarg, argv[i_arg+2]);

        	        	 if (surfsrc.getNumberOfVertices() != surf.getNumberOfVertices())
        	        	 {
        	        		 cerr<<"-sc_resample : incompatible dimensions : "<<surf.getNumberOfVertices()<<" vs "<<surfsrc.getNumberOfVertices()<<endl;
        	        	 }

        	        	 //find common vertices
        	        	 //do forward map
        	        	 unsigned int index1=0;
        	        	 //do forward mapping
        	        	 vector< pair<int,float> > v_closest;
        	        	for (auto v1 = surftarg.const_vbegin(); v1 != surftarg.const_vend(); ++v1,++index1)
        	        	{
              	        	unsigned int index2=0;
        	        		auto v2 = surfsrc.const_vbegin();
        	        		vec3<float> diff = (v1->coord()-v2->coord());
        	        		pair<int,float> closest = pair<int,float>(0,diff.norm());

        	        		for (; v2 != surfsrc.const_vend(); ++v2,++index2)
            	        	{
        	        			diff = (v1->coord()-v2->coord());
        	        			float dist = diff.norm();
//        	        			cout<<"index2 "<<index2<<" "<<dist<<endl;
        	        			if (dist < closest.second)
        	        			{
        	        				closest.first = index2;
        	        				closest.second = dist;
        	        			}
//           	        		cout<<"v1 "<<v1->x<<" "<<v1->y<<" "<<v2->x<<" "<<v2->y<<endl;
//            	        		if ( (v1->x == v2->x ) && (v1->y == v2->y )  && (v1->z == v2->z ) )
//            	        			cout<<"X and Y and Z matches "<<index1<<" "<<index2<<endl;
            	        	}
    	        			v_closest.push_back(closest);

//        	        		cout<<"closest point "<<index1<<" -> "<<closest.first<<" "<<closest.second<<endl;
        	        	}

        	        	//load base surface
          	        	 read_surface(surftarg, argv[i_arg+3]);

        	        	//now resample the scalar data
        	        	//do sequentially for each scalar index

        	        	unsigned int Nverts_targ = surftarg.getNumberOfVertices();
        	        	for (unsigned int sc_index = 0 ; sc_index < surf.getNumberOfScalarData(); ++sc_index)
        	        	{
        	        		vector<float> v_sc_resampled(Nverts_targ,0);
        	        		vector<float> v_sc_src = surf.getScalars(sc_index);
        	        		//loop over all targ verts
//        	        		cout<<"tags N "<<Nverts_targ<<" "<<v_closest.size()<<endl;
        	        		auto i_closest = v_closest.begin();
        	        		for (auto i_sc_resamp = v_sc_resampled.begin(); i_sc_resamp != v_sc_resampled.end(); ++i_sc_resamp,++i_closest )
        	        		{
//        	        			cout<<"clostest "<<i_closest->first<<" "<<v_sc_src[i_closest->first]<<endl;
        	        			*i_sc_resamp = v_sc_src[i_closest->first];//sample closest vertex
        	        		}
            	        	//add scalars to target
//        	        		cout<<"resampled "<<v_sc_resampled[0]<<" "<<v_sc_src[v_closest[0].first]<<endl;
        	        		surftarg.addScalars(v_sc_resampled, surf.getScalarName(sc_index));
        	        	}

        	        	surf=surftarg;

        	            i_arg +=4 ;

        	}else if (command == "-add"){


            fslSurface<float,unsigned int> surf2;// = new fslSurface<float,unsigned int>();
            read_surface(surf2, argv[i_arg+1]);
            
            surf+=surf2;
            i_arg += 2;
            
        }else if (command == "-mul"){
            surf *= string2float(argv[i_arg+1]);
            i_arg += 2;
            cout<<"done multiply"<<endl;
            
        }else if (command == "-div"){
            surf /= string2float(argv[i_arg+1]);
            i_arg += 2;
            
        }else if (command == "-sc_bin"){
            ///            surf /= string2float(argv[i_arg+1]);
            surf.binariseScalars(atoi(argv[i_arg+1]),0.0);
            i_arg += 2;
            
        }else if (command == "-sc_thr"){
            float thresh = string2float(argv[i_arg+1]);
            surf.thresholdScalars(0,thresh);
            i_arg += 2;
            
        }else if (command == "-sc_uthr"){
            float thresh = string2float(argv[i_arg+1]);
            surf.upperThresholdScalars(0,thresh);
            i_arg += 2;
            
        }else if (command == "addVertexIndexAsScalars"){
        	vector<float> v_inds(surf.getNumberOfVertices());
        	unsigned int index=0;
        	for (vector<float>::iterator i_sc = v_inds.begin(); i_sc != v_inds.end(); ++i_sc,++index)
        	{
        		*i_sc = index;
        	}

        	surf.addScalars(v_inds,"vertexIndices");

        	++i_arg;
        }else if (command == "-cluster"){

            cout<<"foudn cluster "<<atoi(argv[i_arg+1])<<" "<<atof(argv[i_arg+2])<<endl;
            fslsurface_name::cluster<float,unsigned int>(surf,atoi(argv[i_arg+1]),atof(argv[i_arg+2]));
            i_arg += 3;
            
        }else if ( command == "-sc_smooth" ){
            
            sc_smooth_mean_neighbour<float,unsigned int>(surf,atoi(argv[i_arg+1]));
            i_arg += 2;
            
        }else if ( command == "-sc_smooth_gauss_geodesic_4D" ){
            
            sc_smooth_gaussian_geodesic<float,unsigned int>(surf,0,string2float(argv[i_arg+1]),string2float(argv[i_arg+1])*4,true);
            i_arg += 2;
            
        }else if (command == "-sc_smooth_gauss_geodesic" ){
            sc_smooth_gaussian_geodesic<float,unsigned int>(surf,atoi(argv[i_arg+1]),string2float(argv[i_arg+2]),string2float(argv[i_arg+2])*4,false);
            i_arg+=3;
            cout<<"done smooth"<<endl;
        }else if (command == "-normals" ){
            surf.calculateNormals(true,false);
            surf.copyNormalsToVectors(0);
            ++i_arg;
        }else if (command == "-projVecOntoNormals"){
            
            projectVectorsOntoNormals(surf,atoi(argv[i_arg+1]));
            i_arg += 2;
            
        }else if (command == "-applyxfm"){
            
            apply_xfm<float, unsigned int>(surf,string(argv[i_arg+1]));
            i_arg += 2;
            
        }else if (command == "-xfmToFSLScaledMM"){
            // surf /= string2float(argv[i_arg+1]);
            vector<double> fmat = surf.getCoordinateSystem("FSL_XFORM_SCALEDMM");
            cout<<"got coordinate systrem "<<endl;
            if (fmat.size() != 16)
            {
                cerr<<"Invalid XFM size.Most likely FSL_XFORM_SCALEDMM does not exist."<<endl;
                exit (EXIT_FAILURE);
            }
            cout<<"xfmToFSLScaledMM Matrix"<<endl;
            // vector<double> fmati = invertMatrix(fmat);
            //  for (vector<double>::iterator i = fmati.begin(); i!= fmati.end();++i)
            //      cout<<*i<<" ";
            //   cout<<endl;
            // print<double>(fmati,"inversematrix");
            apply_xfm<float, unsigned int>(surf,fmat);
            // apply_xfm<float, unsigned int>(&surf,string(argv[i_arg+1]));
            //apply_xfm<float, unsigned int>(&surf,fmati);
            
            cout<<"done apply"<<endl;
            ++i_arg;// += 2;
            
        }else if (command == "-applyxfmFlirt"){
            // surf /= string2float(argv[i_arg+1]);
            vector<double> fmat = surf.getCoordinateSystem("FSL_XFORM_SCALEDMM");
            cout<<"got coordinate systrem "<<endl;
            if (fmat.size() != 16)
            {
                cerr<<"Invalid XFM size.Most likely FSL_XFORM_SCALEDMM does not exist."<<endl;
                exit (EXIT_FAILURE);
            }
            cout<<"invert Matrix"<<endl;
            volume<float> imref;
            read_volume_hdr_only(imref, argv[i_arg+2]);
            
            vector<float> xfm = getFSLtoNIFTIxfm(imref);
            
            
            // vector<double> fmati = invertMatrix(fmat);
            //for (vector<double>::iterator i = fmati.begin(); i!= fmati.end();++i)
            //   cout<<*i<<" ";
            //cout<<endl;
            // print<double>(fmati,"inversematrix");
            apply_xfm<float, unsigned int>(surf,fmat);
            apply_xfm<float, unsigned int>(surf,string(argv[i_arg+1]));
            apply_xfm<float, unsigned int>(surf,xfm);
            
            
            i_arg += 3;
            
        }else if (command == "-reg_lq"){
            cout<<"reglq"<<endl;
            // surf /= string2float(argv[i_arg+1]);
            fslSurface<float,unsigned int> surf2;// = new fslSurface<float,unsigned int>();
            cout<<"read surface "<<argv[i_arg+1]<<endl;
            read_surface(surf2, argv[i_arg+1]);
            unsigned int dof = atoi(argv[i_arg+2]);
            
            cout<<"do mesh reg"<<endl;
            //do registration
            vector<float> xfm = meshRegLeastSq( surf, surf2, dof );
            cout<<"xfm4x4 "<<xfm.size()<<endl;
            //save linear xfm
            ofstream fmat_out(argv[i_arg+3]);
            int col=1;
            for (vector<float>::iterator i = xfm.begin(); i != xfm.end(); ++i,++col)
            {
                if ( (col % 4) ==0)
                {
                    fmat_out<<*i<<endl;
                }else {
                    fmat_out<<*i<<" ";
                }
            }
            //applyxfm to vertices
            apply_xfm<float, unsigned int,float>(surf,xfm);
            surf.copyCoordSystems(surf2);
            i_arg += 4;
            
        }else if ( command == "-copy_CoordSystems" )
        {
            fslSurface<float,unsigned int> surf2;
            cout<<"read surface "<<argv[i_arg+1]<<endl;
            read_surface(surf2, argv[i_arg+1]);
            surf.copyCoordSystems(surf2);
            
            i_arg += 2;
        }else if (command == "-copy_verts"){
            fslSurface<float,unsigned int> surf2;// = new fslSurface<float,unsigned int>();
            read_surface(surf2, argv[i_arg+1]);
            surf.copyVertices(surf2);
            //surf+=surf2;
            i_arg += 2;
            
        }else if (command == "-copy_verts_from_text"){
			//read in vertcies
			ifstream fv(argv[i_arg+1]);
			if (fv.is_open())
			{
				vector<float> verts(surf.getNumberOfVertices()*3,0);

				string line;
				auto i_v = verts.begin(); 
				while (getline(fv,line) )
				{
					stringstream ss(line);
					float x,y,z;
					ss>>x>>y>>z;
					*(i_v++)=x;
					*(i_v++)=y;
					*(i_v++)=z;					
					
				}
					   cout<<"verts "<<endl;					
					   for (auto i = verts.begin(); i != verts.end(); ++i)
					   {
						   cout<<(*i)<<" ";
					   }
					   cout<<endl;
					   fv.close();
				surf.setVertices(verts);

			}
			
            //surf+=surf2;
            i_arg += 2;
            
        }else if (command == "-copy_verts2vec"){
            surf.copyVerticesToVectors();
            ++i_arg;
        }else if (command == "-copy_scalar2image3D"){
            volume<float> image(surf.getNumberOfVertices(),1,1);
            unsigned int scind = atoi(argv[i_arg+1]);
            
            unsigned int index=0;
            for (std::vector< float >::const_iterator i_sc = surf.const_scbegin(scind); i_sc != surf.const_scend(scind); ++i_sc,++index)
            {
                image.value(index,0,0) = *i_sc;
            }
            
            save_volume(image, argv[i_arg+2]);
            i_arg += 3;
            if (i_arg == argc)
                return 0;
            
        }else if (command == "-copy_scalars2image4D"){
            unsigned int Nt=surf.getNumberOfScalarData();
            volume4D<float> image(surf.getNumberOfVertices(),1,1,Nt);
            //            unsigned int scind = atoi(argv[i_arg+1]);
            
            for ( unsigned int scind = 0 ; scind < Nt;++scind)
            {
                unsigned int index=0;
                for (std::vector< float >::const_iterator i_sc = surf.const_scbegin(scind); i_sc != surf.const_scend(scind); ++i_sc,++index)
                {
                    image[scind].value(index,0,0) = *i_sc;
                }
            }
            save_volume4D(image, argv[i_arg+1]);
            i_arg += 2;
            if (i_arg == argc)
                return 0;
            
        }else if (command == "-copy_vec2image3D"){
            cout<<"FOUND copy_vec2image3D "<<endl;
            unsigned int N = surf.getNumberOfVertices();
            volume<float> image(N,3,1);
            unsigned int vecind = atoi(argv[i_arg+1]);
            unsigned int index=0;
            for (vector<float>::const_iterator i_vec = surf.const_vecbegin(vecind); i_vec != surf.const_vecend(vecind); ++i_vec,++index)
            {
                image.value(index,0,0)=*i_vec;
                ++i_vec;
                image.value(index,1,0)=*i_vec;
                ++i_vec;
                image.value(index,2,0)=*i_vec;
                
                
            }
            cout<<"save image "<<argv[i_arg+2]<<endl;
            save_volume(image, argv[i_arg+2]);
            
            i_arg += 3;
            if (i_arg == argc)
                return 0;
            
        }else if (command == "-vmag"){
            cout<<"FOUND vmag "<<endl;
            unsigned int N = surf.getNumberOfVertices();
            unsigned int vecind = atoi(argv[i_arg+1]);
            vector<float> sc(N);
            vector<float>::iterator i_sc = sc.begin();
            for (vector<float>::const_iterator i_vec = surf.const_vecbegin(vecind); i_vec != surf.const_vecend(vecind); ++i_vec,++i_sc)
            {
                float x = *i_vec;
                ++i_vec;
                float y = *i_vec;
                ++i_vec;
                float z = *i_vec;
                *i_sc = sqrtf( x*x + y*y + z*z );
                
            }
            surf.insertScalars(sc, 0, argv[i_arg+2]);
            //   surf.addScalars(sc, argv[i_arg+2]);
            i_arg += 3;
            
        }else if (command == "-copy_image3D2scalar"){
            unsigned int N = surf.getNumberOfVertices();
            volume<float> image;
            read_volume(image,argv[i_arg+1]);
            if ((static_cast<unsigned int>(image.xsize())!=N) || (static_cast<unsigned int>(image.ysize())!=1) || (static_cast<unsigned int>(image.zsize())!=1))
            {
                cerr<<"Error : copy_image3D2scalar : Incompatible image dimensions "<<endl;
                exit (EXIT_FAILURE);
            }
            
            
            vector<float> sc(N);
            vector<float>::iterator i_sc = sc.begin();
            for ( unsigned int x = 0 ; x < N; ++x,++i_sc)
            {
                *i_sc = image.value(x,0,0);
                cout<<"sc "<<*i_sc<<endl;
                
            }
            cout<<"insert the scalars"<<endl;
            surf.insertScalars(sc,0,argv[i_arg+2]);
            i_arg += 3;
            
        }else if (command == "-copy_image4D2scalar"){
            unsigned int N = surf.getNumberOfVertices();
            volume4D<float> image;
            read_volume4D(image,argv[i_arg+1]);
            if ((static_cast<unsigned int>(image.xsize())!=N) || (static_cast<unsigned int>(image.ysize())!=1) || (static_cast<unsigned int>(image.zsize())!=1))
            {
                cerr<<"Error : copy_image4D2scalar : Incompatible image dimensions "<<endl;
                exit (EXIT_FAILURE);
            }
            insertScalars<float,unsigned int,float>(surf,image,argv[i_arg+2]);
            
            /*
             vector<float> sc(N);
             vector<float>::iterator i_sc = sc.begin();
             for ( unsigned int x = 0 ; x < N; ++x,++i_sc)
             {
             *i_sc = image.value(x,0,0);
             cout<<"sc "<<*i_sc<<endl;
             
             }
             cout<<"insert the scalars"<<endl;
             surf.insertScalars(sc,0,argv[i_arg+2]);
             */
            i_arg += 3;
            
        }else if (command == "-copy_image3D2vec"){
            cout<<"FOUND copy_image3D2vec "<<endl;
            unsigned int N = surf.getNumberOfVertices();
            volume<float> image;
            read_volume(image,argv[i_arg+1]);
            cout<<"N "<<N<<" "<<image.xsize()<<" "<<image.ysize()<<" "<<image.zsize()<<endl;
            
            if ((static_cast<unsigned int>(image.xsize())!=N) || (static_cast<unsigned int>(image.ysize()) !=3) || (static_cast<unsigned int>(image.zsize())!=1))
            {
                cerr<<"Error : copy_image3D2vec : Incompatible image dimensions "<<endl;
                exit (EXIT_FAILURE);
            }
            
            //(N,3,1);
            vector<float> vec(N*3);
            
            
            string comp = string(argv[i_arg+4]);
            bool cpx = false;
            bool cpy = false;
            bool cpz = false;
            
            for ( unsigned int i =0 ; i<comp.length();++i)
            {
                string c = comp.substr(i,1);
                cout<<"comp "<<c<<endl;
                if (c=="x")
                    cpx=true;
                else if (c=="y")
                    cpy=true;
                else if (c=="z")
                    cpz=true;
            }
            
            
            vector<float>::iterator i=vec.begin();
            for ( unsigned int x = 0 ; x < N; ++x)
            {
                if (cpx)
                    *i=image.value(x,0,0);
                else
                    *i=0;
                ++i;
                if (cpy)
                    *i=image.value(x,1,0);
                else
                    *i=0;
                ++i;
                if (cpz)
                    *i=image.value(x,2,0);
                else
                    *i=0;
                ++i;
                
            }
            unsigned int vecind = atoi(argv[i_arg+3]);
            
            surf.addVectors(vec, argv[i_arg+2], vecind);
            i_arg += 5;
            
        }else if ( command == "-sampleTimeSeries" ) {
            volume4D<float> image;
            read_volume4D(image,argv[i_arg+1]);
            
            surf.clearScalars();
            sampleTimeSeries(surf , image, argv[i_arg+2] );
            i_arg += 3;
            
        }else if ( command == "-sampleImage" ) {
            volume<float> image;
            read_volume(image,argv[i_arg+1]);

            surf.clearScalars();
            sampleImage(surf , image, argv[i_arg+2],  argv[i_arg+3]);
            i_arg += 4;

        }else if ( command == "-writeTimeSeries" ){
            volume4D<float> image;
            read_volume4D(image,argv[i_arg+1]);
            
            writeTimeSeries( surf , image , argv[i_arg+2]);
            i_arg += 3;
            
            
        }else if ( command == "-marchingCubes"  ){
            volume<float> image;
            read_volume(image,argv[i_arg+1]);
            image_dims dims(image.xsize(), image.ysize(), image.zsize(), image.xdim(), image.ydim(), image.zdim());
            marchingCubes<float,unsigned int, float>( surf, image.fbegin(),  dims, atof(argv[i_arg+2]), atof(argv[i_arg+3]), static_cast<MarchingCubesMode>(atof(argv[i_arg+4])));
            i_arg += 5;
            
        }else if (command == "-reconFromBvars"){
            cout<<"do recon "<<endl;
            char* fsldir = getenv("FSLDIR");
            if (fsldir == NULL)
            {
                cerr<<"FSLDIR has not been set. "<<endl;
                exit(EXIT_FAILURE);
            }
            //file.bvars,mni_template.nii.gz
            // string mni = string(fsldir)+"/data/standard/MNI152_T1_1mm";
            reconSurface_from_bvars( surf, string(argv[i_arg+1]));
            i_arg += 2;
            cout<<"done recon"<<endl;
            
        }else if (command == "-reconAllFromBvarsAndSave"){
            cout<<"do recon+save "<<argc<<" "<<i_arg<<endl;
            char* fsldir = getenv("FSLDIR");
            if (fsldir == NULL)
            {
                cerr<<"FSLDIR has not been set. "<<endl;
                exit(EXIT_FAILURE);
            }
            //file.bvars,mni_template.nii.gz
            // string mni = string(fsldir)+"/data/standard/MNI152_T1_1mm";
            cout<<"recon "<< string(argv[i_arg+1])<<endl;
            cout<<" "<<string(argv[i_arg+2])<<endl;
            reconAllSurfacesAndSave( string(argv[i_arg+1]), string(argv[i_arg+2]));
            i_arg += 3;
            cout<<"done recon"<<endl;
            return 0;
        }else if ( command == "-FtoP" ){
            cout<<"do FtoP"<<endl;
            vector<int> fdofs=surf.getNonVertIntScalars("FStatDOFs");
            
            cout<<"got dofs "<<fdofs.size()<<endl;
            int df1=fdofs[0];
            int df2=fdofs[1];
            cout << "FStat DOFs = " << df1 << " and " << df2 << endl;
            
            surf.insertScalars(FtoP(surf.getScalars("F-stats_EV_1"),df1,df2),0,"FtoP");
            
            ++i_arg;
            
            
		}else if ( command == "-vertexMVGLM" ){
            fslSurface<float,unsigned int> *surf2 = new fslSurface<float,unsigned int>();
            //list of surfaces, deign matrx name, save vertices, norm name
            cout<<"dovertex glm"<<endl;
            vertexMVglm( *surf2, string(argv[i_arg+1]),  string(argv[i_arg+2]), "",string(argv[i_arg+3]));
            cout<<"done glm"<<endl;
            surf=*surf2;
            delete surf2;
            i_arg += 4;
            
        }else if ( command == "-warp" ){
            volume4D<float> im_warp;
            cout<<"warp file "<<argv[i_arg+1]<<endl;
            read_volume4D(im_warp,argv[i_arg+1]);
            apply_warp(surf, im_warp);
            i_arg += 2;
            
        }else if ( command == "-deform" ){
            cout<<"deform"<<endl;
            //    int win = glutInitAndCreateWindow(&argc,argv,argv[i_arg+1]);
            //      glutDisplayFunc(glutDisp);
            //  glutIdleFunc(glutIdle);
            // glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            //glutKeyboardFunc(keyb);
            glutRender(surf);
            //  glBegin(GL_TRIANGLES);
            //glVertex3f(-0.5,-0.5,0.0);
            //glVertex3f(0.5,0.0,0.0);
            //glVertex3f(0.0,0.5,0.0);
            //glEnd();
            //            glutMainLoop();
            //  glutDestroyWindow(win);
            //  glutSwapBuffers();
            cout<<"done deform "<<endl;
            i_arg+=2;
        }else if ( command == "-fillMesh" ){
            
            
            int lb = atoi(argv[i_arg+2]);
            
            
            volume<short> mask;
            
            read_volume(mask,argv[i_arg+1]);
            mask=0;
            mask=fillMesh( surf, mask, lb);
            save_volume(mask,argv[i_arg+3]);
            
            i_arg += 4;

        }else if ( command == "-drawMesh" ){
            
            
            int lb = atoi(argv[i_arg+2]);
     
            volume<short> mask;
            
            read_volume(mask,argv[i_arg+1]);
            mask=0;
            draw_mesh( surf, mask, lb);
            save_volume(mask,argv[i_arg+3]);
            
            i_arg += 4;
        }
        
        
        else if ( command == "-appendFreesurferLabelFile" )
        {
            ifstream fin(argv[i_arg+1]);
            if (! fin.is_open()){
                cerr<<"Failed to open file "<<argv[i_arg+1]<<endl;
            }
            
            string file;
            while (getline(fin,file))
            {
                cout<<"Appending "<<file<<"..."<<endl;
                if ( appendFreesurferLabel(surf,file) )
                {
                    cerr<<"Failed to append file "<<file<<endl;
                    exit (EXIT_FAILURE);
                    
                }
                
            }
            
            
            fin.close();
            i_arg += 2;
        }else if ( command == "-splitFreesurferLabelFiles" )
        {
            ifstream fin(argv[i_arg+1]);
            if (! fin.is_open()){
                cerr<<"Failed to open file "<<argv[i_arg+1]<<endl;
            }

            string lb_name = argv[i_arg+2];
            //only unidirectional indices to prevent duplicate inndices
        	surf.computeAdjacentVertices(1);

            string file;

            while (getline(fin,file))
            {
                cout<<"Reading Freesurfer File "<<file<<"..."<<endl;
                //process label file
            	std::size_t found = file.find_last_of("/\\");
            	std::size_t foundend = file.find_last_of(".");
            	string filebase;
            	//Need to handle the case where there is no path preceding the filenames
        		if (found == string::npos)
        		{
        			filebase= file.substr(0, foundend-found-1);
        		}else{
        			filebase= file.substr(found+1, foundend-found-1);
        		}
        		cout<<"filebase "<<file<<" "<<filebase<<" "<<found<<" "<<foundend<<" "<<file.size()<<endl;


                ifstream flabel(file);
                if (flabel.is_open())
                {
                	string header;
                	getline(flabel,header);
                	string s_Nverts;
                	getline(flabel,s_Nverts);
                	unsigned int Nverts = stoul(s_Nverts);
                	cout<<"Number of vertices "<<Nverts<<endl;
                	list<unsigned int> l_vert_inds;
                	vector< pair<unsigned int,string> > v_ind2line;
                	string line;
                	while (getline(flabel,line))
                	{
                		unsigned int ind;
                		stringstream ss(line);
                		ss>>ind;
                		l_vert_inds.push_back(ind);
                		v_ind2line.push_back(pair<unsigned int,string>(ind,line));

                	}
                	cout<<"Nverts_l "<<l_vert_inds.size();

                	//now lets do the connectivity
                	//label files are stored in ascednign index order
                	//first

                	vector< list<unsigned int> > vert_adj = surf.getVertexAdjacencies();

                	vector< list<unsigned int> > all_roi;
                	//the current subroi we'll be adding to

                	list<unsigned int> l_cur_roi;
                	//add first vertex
                	l_cur_roi.push_back(l_vert_inds.front());
                	l_vert_inds.pop_front();

                	list<unsigned int>::iterator i_cur_roi = l_cur_roi.begin();
                	//check if adjacent vertices are members of ROI
                	unsigned int cur_vert;
                	while (! l_vert_inds.empty()){
                		cur_vert = *i_cur_roi;
//                		             		cout<<"vrtex "<<cur_vert<<endl;
//                		             		cout<<"front "<<l_vert_inds.front()<<endl;
//                		             		cout<<"adj: ";
                		for (auto nbour = vert_adj[cur_vert].begin(); nbour != vert_adj[cur_vert].end(); ++nbour)
                		{
                			//check if neighbours are part or ROI, then remove it and pop onto our current ROI list
//                			cout<<(*nbour)<<" ";
                			if (search_and_remove_from_ordered_list(l_vert_inds,*nbour))
                			{
//                				cout<<"pushback "<<endl;
                				l_cur_roi.push_back(*nbour);//add to list of rois to search neighbours
                				//                			cout<<"found in list "<<search_and_remove_from_ordered_list(l_vert_inds,*nbour)<<endl;
                			}else{
//                				cout<<endl;
                			}
                		}
//                		             		cout<<endl;
                		//increment the current roi iterator
                		//                		cout<<endl<<"vertices remaining/newroi "<<l_vert_inds.size()<<" "<<l_cur_roi.size()<<endl;
                		++i_cur_roi;
                		if (i_cur_roi == l_cur_roi.end() )
                		{
                			cout<<endl<<"Done ROI : "<<l_cur_roi.size()<<endl;
                			all_roi.push_back(l_cur_roi);
                			l_cur_roi.clear();//clear exisintg
                			//initialize next roi within the label
                			if (! l_vert_inds.empty() )
                			{
                		    	l_cur_roi.push_back(l_vert_inds.front());
                		                	l_vert_inds.pop_front();
                		                	i_cur_roi=l_cur_roi.begin();
                			}
                			//                			break;
                		}
                	}

                	//need to account for any left over veritices at the end
                	if (! l_cur_roi.empty())
                	{
            			all_roi.push_back(l_cur_roi);
                	}
                	cout<<endl;
                	flabel.close();

                	//lets filter out label file
                	unsigned int index=0;
                	for (auto i_all = all_roi.begin(); i_all != all_roi.end(); ++i_all,++index)
                	{
                		cout<<"Sorting veretx indices : "<<index<<endl;
                		i_all->sort();//the sort will cailitate the write algorithm
                		cout<<"Writing "<<(lb_name + "/" + filebase +"_" + to_string(index) +".label")<<"...";
                		ofstream flb_out((lb_name + "/" + filebase +"_" + to_string(index) +".label").c_str());

                		if (flb_out.is_open()){
                			flb_out<<header<<endl;
                			flb_out<<i_all->size()<<endl;

                			//this algorithm assumes the orderedness is preserved

                			auto i_roi = i_all->begin();
                			for (auto i_line = v_ind2line.begin(); i_line != v_ind2line.end(); ++i_line)
                			{
                				//                			cout<<"line "<<i_line->first<<" "<<(*i_roi)<<" "<<i_line->second<<endl;
                				if (i_line->first == *i_roi )
                				{
                					flb_out<<i_line->second<<endl;
                					++i_roi;
                				}
                			}
                			flb_out.close();

                		}
                		cout<<"done."<<endl;

                	}




                }else {
                	cout<<"failed to open output file "<<file<<endl;
                			//end if statement for open file
                }

            }//end while loop over label file


            fin.close();
            i_arg += 3;

        }else{
            
            cerr<<"Unrecognized command "<<command<<endl;
            Usage();
            exit(EXIT_FAILURE);
        }
    }
    
    
//    cout<<"do write "<<i_arg<<" "<<argc<<" "<<argv[i_arg]<<" "<<surf.getNumberOfCoordinateSystems()<<endl;
    if (surf.getNumberOfCoordinateSystems() == 0 )
    {
        vector<double> identity(16,0);
        identity[0] = identity[5] = identity[10] = identity[15] = 1.0;
        surf.addCoordSystem(identity, "NIFTI_XFORM_UNKNOWN");
    }
    
    if (i_arg > argc)
    {
        cerr<<"Did not specify output name"<<endl;
        exit (EXIT_FAILURE);
    }
    
    if ( i_arg < argc )
    {
        
        cout<<"write gifti"<<endl;
        writeGIFTI(surf, string(argv[i_arg]));
        cout<<"done writing gifti"<<endl;
        cout<<"write vtk"<<endl;
//        Mesh* m = new Mesh();
//        convertTo_Mesh(m,&surf);
//        m->save(string(argv[i_arg])+".vtk",3);
        
//        delete m;
    
    }else{
        cout<<"not surface to write to"<<endl;
    }
    //  delete surf;
    return 0;
}

