#ifndef FSLSURFACEIO_H
#define FSLSURFACEIO_H

//#include <fslsurface.h>

#include <iostream>
#include <vector>


namespace fslsurface_name {
    template<class T, class T2>
    class fslSurface;
    
    enum FileType { Unknown, PLY, GIFTI, VTK };
    //	class fslSurfaceIO {
    //	public:
    //-----------------Read Functions--------------------------//
    template<class T, class T2>
    int read_surface( fslSurface<T,T2>& surf, const std::string & filename);
    template<class T, class T2>
    int readGIFTI( fslSurface<T,T2>& surf, const std::string & filename );
    template<class T, class T2>
    int readVTK( fslSurface<T,T2>& surf, const std::string & filename );
    template<class T, class T2>
    int writeVTK( const fslSurface<T,T2> & surf, const std::string & filename);
    
    template<class T, class T2>
    int readPLY( fslSurface<T,T2>& surf, const std::string & filename );
    //--------------------------------------------------//
    template<class T, class T2>
    int writePLYHeader( const fslSurface<T,T2>& surf, std::ofstream & fout,const std::string & version, const std::string & comment);
    template<class T, class T2>
    int writePLY( const fslSurface<T,T2>& surf, const std::string & filename);
    template<class T, class T2>
    int readPLYHeader( const fslSurface<T,T2>& surf, std::ifstream & fin);
    template<class T, class T2>
    unsigned int readPLYVertices( fslSurface<T,T2>& surf, std::ifstream & fin , const unsigned int & index);
    
    template<class T,class T2>
    std::vector<T> readPLYElement(  fslSurface<T,T2>& surf, std::ifstream & fin , const unsigned int & index);
    
    template<class T,class T2>
    std::vector<T> readPLYListElement(  fslSurface<T,T2>& surf, std::ifstream & fin , const unsigned int & index, unsigned int & list_index);
    
    template<class T,class T2>
    int writeGIFTI(  const fslSurface<T,T2>& surf, const std::string & filename);
    
    template<class T,class T2>
    int appendFreesurferLabel(  fslSurface<T,T2> & surf, const std::string & filename);
    //	};
    
    
    //--------------THIS PORTION IS FOR FIRST I/O INCLUDING MODELS-------------------//
    
    
    
}
#endif