#include <fslsurface.h>
#include <iostream>
#include "newimage/newimageall.h"
#include <fslsurface_structs.h>
#include <fslsurfacefns.h>
#include <fslsurfaceio.h>

using namespace fslsurface_name;
using namespace std;
using namespace NEWIMAGE;


int main( int argc, const char* argv[] )
{
	//input 1: image
	//inout 2: threshold
	//input 3: scalar value
	//input 4 discreet mode
	//input 3: output
	//input 5: marching mode : 0 = equal_to ; 1= greater_than ; 2= greater_than_equal_to
	if (argc < 6 )
	{
		cerr<<"Wrong inputs "<<argc<<endl;
		return 1;
	}
	volume<float> image;
	read_volume(image,argv[1]);
//	image.threshold(atof(argv[2]));
	image_dims dims(image.xsize(), image.ysize(), image.zsize(), image.xdim(), image.ydim(), image.zdim());
	fslSurface<float,unsigned int> surface;// = new fslSurface<float,unsigned int>();
	
	if (atoi(argv[4]) == 0 ){//discrete labels case
		runMarchingCubesOnAllLabels<float,unsigned int, float>(surface, image.fbegin(), dims,atof(argv[2]));//, atof(argv[3]));

	}else {
		cout<<"run marching cubes "<<atof(argv[2])<<" "<<atof(argv[3])<<" "<<atoi(argv[4])<<endl;
		marchingCubes<float,unsigned int,float>(surface, image.fbegin(), dims,atof(argv[2]), atof(argv[3]),static_cast<MarchingCubesMode>(atoi(argv[4])));

		}

	
	writeGIFTI(surface, argv[5]);
//	delete surface;
	return 0;
}
