# $Id: Makefile,v 1.12 2011/09/06 07:26:53 brian Exp $
include ${FSLCONFDIR}/default.mk

PROJNAME = fslsurface


#CC = llvm-gcc
#CXX = llvm-g++
#ARCHFLAGS = -arch x86_64
#ARCHLDFLAGS = -arch x86_64
#CXX += -Wc++11-extensions
CXXFLAGS+= -Wall -O -std=c++11

#ifeq ($(FSLMACHTYPE),apple-darwin8-gcc4.0)
#        ARCHFLAGS = -arch ppc -arch i386 -isysroot /Developer/SDKs/MacOSX10.4u.sdk -I/usr/X11R6/include/
#        ARCHLDFLAGS = -Wl,-search_paths_first -arch ppc -arch i386 -isysroot /Developer/SDKs/MacOSX10.4u.sdk -L/Developer/SDKs/MacOSX10.4u.sdk/usr/X11R6/lib/
#endif

#INC_NEWMAT=./newmat
#LIB_NEWMAT=./newmat


USRINCFLAGS = -I${INC_NEWMAT} -I${INC_PROB} -I${INC_ZLIB} -I${FSLDIR}/include/niftiio 

USRLDFLAGS = -L${LIB_NEWMAT}  -L${INC_PROB} -L${LIB_ZLIB} -L./ 


LIBS=-lgiftiio -lexpat  -lmeshclass
FMLIBS= -lshapeModel -lfirst_lib -lfslvtkio -lmeshclass -lnewimage -lmiscmaths  -lfslio -lniftiio -lznz -lnewmat -lutils -lprob -lz
openGL= -framework GLUT -framework openGL

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
	GNU_ANSI_FLAGS += -DARCHLINUX
	CXX_CFLAGS += -DARCHLINUX
	openGL=-lglut -lGL
else
	openGL= -framework GLUT -framework openGL
endif


APPLY_ZLIB = -DHAVE_ZLIB


SHAPEOBJS=fslsurface.o 
MODELOBJS=fslsurface.o


LIBFILES=fslsurface.a fslsurfacegl.a fslsurface_backcompat.a
XFILES=fslsurfacemaths marching_cubes

all:  ${LIBFILES} ${XFILES}

fslsurface.a: fslsurfacefns.o fslsurfaceio.o fslsurface.o 
	${AR} -r libfslsurface.a  fslsurfacefns.o fslsurfaceio.o fslsurface.o
fslsurfacegl.a: fslsurfacegl.o fslsurface.a
	${AR} -r libfslsurfacegl.a fslsurfacegl.o
fslsurface_backcompat.a: fslsurface_first.o fslsurface_dataconv.o fslsurface.a
	${AR} -r libfslsurface_backcompat.a fslsurface_first.o fslsurface_dataconv.o
fslsurfacemaths: fslsurface.a fslsurfacemaths.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ fslsurfacemaths.o fslsurfaceio.o fslsurfacefns.o fslsurface_first.o fslsurfacegl.o fslsurface_dataconv.o fslsurface.o ${LIBS} ${FMLIBS} ${openGL}
marching_cubes: fslsurface.a marching_cubes.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ marching_cubes.o fslsurfaceio.o fslsurfacefns.o fslsurface.o ${LIBS} ${FMLIBS} ${openGL}
